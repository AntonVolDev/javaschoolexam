package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.Stack;

public class Calculator {


    Stack<Double> stack = new Stack<>();
    String postfix = "";


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {

        if (statement == null) return null; // Check argument for null

        PostfixForm convertor = new PostfixForm(statement); // Transform to postfix form
        postfix = convertor.toPostfix();

        double value;
        double tmpResult = 0;
        double num1, num2;

        if(postfix.isEmpty()) return null;  //  Check postfix equality to emptiness

        String[] tmp = postfix.split(" ");
        for(int j=0; j<tmp.length; j++){
            if(tmp[j].isEmpty())
                return null;
            // if it is not an operator
            if(!tmp[j].equals("+") && !tmp[j].equals("-") &&
                    !tmp[j].equals("*") && !tmp[j].equals("/")){
                try{
                    value = Double.valueOf(tmp[j]);
                }catch(NumberFormatException ex){
                    //  If it is not a digit return null
                    return null;
                }
                stack.push(value);
            }else{
                //  If it is an operator - make calculation
                num2 = Double.valueOf(stack.pop());
                num1 = Double.valueOf(stack.pop());
                if(tmp[j].equals("+")){
                    tmpResult = num1 + num2;
                }
                if(tmp[j].equals("-")){
                    tmpResult = num1 - num2;
                }
                if(tmp[j].equals("*")){
                    tmpResult = num1 * num2;
                }
                if(tmp[j].equals("/")){
                    //  Divide by zero is forbidden
                    if(num2 == 0)
                        return null;
                    tmpResult = num1 / num2;
                }
                stack.push(tmpResult);
            }
        }

        //  Pretty view ouput
        DecimalFormat df = new DecimalFormat("#.####");
        return df.format(stack.pop()).toString();
    }
}
