package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

/**
 * This class consists methods for transforming and processing input String statement
 * to postfix form notation
 */
public class PostfixForm {

    private Stack<Character> stack;
    private String input;
    private String output = "";


    public PostfixForm(String input)
    {
        this.input = input;
        stack = new Stack<>();
    }


    /**
     * The method trasform input String statement to postfix form notation
     */
    public String toPostfix()
    {
        for (int j = 0; j < input.length(); j++) {
            char ch = input.charAt(j);
            if(ch == ' ')
                continue;
            if(ch == '+' || ch == '-'){
                output += ' ';
                //  Priority of the operations is 1
                getOper(ch, 1);
                continue;
            }
            if(ch == '*' || ch == '/'){
                output += ' ';
                //  Priority of the operations is 2
                getOper(ch, 2);
                continue;
            }
            if(ch == '('){
                stack.push(ch);
                continue;
            }
            if(ch == ')'){
                int res = getParent(ch);
                if(res == 0) return "";
            } else {
                //  Write to output
                output += ch;
            }


        }
        //  Pop remaining operators
        while (!stack.isEmpty())
        {
            output += ' ';
            //  Write to output
            output += stack.pop();
        }
        //  Return postfix
        return output;
    }

    /**
     * Method for processing operand
     * @param opThis
     * @param prec1
     */
    public void getOper(char opThis, int prec1) {
        //  Get operator from input
        while (!stack.isEmpty()) {
            char opTop = stack.pop();
            //  If it is a '(' - push it
            if (opTop ==  '(') {
                stack.push(opTop);
                break;
            }else{
                // Operator for comparing precedence
                int prec2;
                // Find precedence
                if (opTop == '+' || opTop == '-')
                    prec2 = 1;
                else
                    prec2 = 2;
                // If precedence less
                if (prec2 < prec1){
                    stack.push(opTop);
                    break;

                }else{
                    // If precedence greater
                    output = output + opTop + ' ';
                }
            }
        }
        stack.push(opThis);     // push new operator
    }


    /**
     * Method for processing parentheses
     * @param ch
     * @return
     */
    public int getParent(char ch) {

        // get right ')' from input
        //if stack has few than 2 elements
        //equation is incorrect
        if(stack.size() < 2)
            return 0;
        while (!stack.isEmpty()) {
            char chx = stack.pop();
            if (chx == '(') {
                break;
            }else{
                output += ' ';
                output += chx; // output it
            }
        }
        return 1;
    }

}
