package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Pyramid can't be built with given input";
    }
}
