package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        //  Check input List for equality to null, for null in List elements and for List equality to emptiness
        if (inputNumbers == null || inputNumbers.contains(null) || inputNumbers.isEmpty())
            throw new CannotBuildPyramidException();

        try {
            //  Sort numbers in input List
            Collections.sort(inputNumbers);

            //  ArrayList with a eligible size according input List length and formula: ((N+1)/2)
            ArrayList<Integer> correctList = new ArrayList<>();
            for (Integer i = 1; i <= inputNumbers.size(); i++) {
                correctList.add(i * (i + 1) / 2);
            }

            //  Define the size of the "pyramid" array
            int rows = correctList.indexOf(inputNumbers.size()) + 1;
            int columns = 2 * correctList.indexOf(inputNumbers.size()) + 1;

            //  Create "pyramid" array for real input values
            int[][] resultArray = new int[rows][columns];
            int index = 0;

            //  Fill the array starting with position N-i-1
            for (int i = 0; i < rows; i++) {
                for (int j = rows - i - 1, k = 1; k <= i + 1; j += 2, k++) {
                    resultArray[i][j] = inputNumbers.get(index);
                    index++;
                }
            }
            return resultArray;


        } catch (Throwable t) {
            throw new CannotBuildPyramidException();
        }
    }
}