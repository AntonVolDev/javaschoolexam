package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        //  Check for null input arguments value
        if (x == null || y == null)
            throw new IllegalArgumentException();

        //  Check for empty 1-st case (both List are empty)
        if (x.isEmpty() && y.isEmpty())
            return true;

        //  Check for empty 2-nd case (only x List is empty)
        else if (x.isEmpty() && !y.isEmpty())
            return true;

        //  Iterators for two input Lists
        Iterator<Object> xListIterator = x.iterator();
        Iterator<Object> yListIterator = y.iterator();

        //  Current element Objects from two Lists
        Object xCurrentObject;
        Object yCurrentObject;
        ;

        //  Get first element from x List
        xCurrentObject = xListIterator.next();

        //  Check if y List current element Object contains x List current element Object
        while (yListIterator.hasNext()) {
            //  Get next element Object from y List
            yCurrentObject = yListIterator.next();

            //  Check for equality two current Object from both List
            if (yCurrentObject.equals(xCurrentObject)) {
                if (xListIterator.hasNext()) {
                    //  Get next element Object from x List
                    xCurrentObject = xListIterator.next();
                } else {
                    //  If List x has't more elements Object
                    return true;
                }
            }
        }

        return false;
    }
}
